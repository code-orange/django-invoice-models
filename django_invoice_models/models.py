from django.db import models


class InvoiceSettings(models.Model):
    name = models.CharField(max_length=200, unique=True)
    value = models.CharField(max_length=200)

    class Meta:
        db_table = "invoice_settings"


class InvoiceSettingsData(models.Model):
    name = models.CharField(max_length=200, unique=True)
    value = models.BinaryField()

    class Meta:
        db_table = "invoice_settings_data"
