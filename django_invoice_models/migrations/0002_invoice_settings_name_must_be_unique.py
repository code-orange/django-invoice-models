# Generated by Django 3.1.14 on 2022-01-11 09:53

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_invoice_models", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="invoicesettings",
            name="name",
            field=models.CharField(max_length=200, unique=True),
        ),
    ]
